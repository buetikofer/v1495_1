-- ****************************************************************************
-- Model:           V1495 -  Multipurpose Programmable Trigger Unit
-- Device:          ALTERA EP1C4F400C6
-- Author:          Lukas Buetikofer (lukas.buetikofer@lhep.unibe.ch)
-- Date:            16-07-2015
-- ----------------------------------------------------------------------------
-- Module:          MainLogicUser
-- Description:     Firmware for v1495 used in XENON1T DAQ.
-- 
--						  This firmware takes the LVDS busy signals from the V1724 digitizers at the input A
--						  as well as the signal from the DDC-10 HE-veto (used for XENON1T) at input G_0
--						  and combines them usign a logic OR.
--						  The output of this logic connection is sent to F_2.
--						  Further more the busy and the HEV NIM signals are encoded according the following scheme:
--						  One short pulse at falling edge and two short pulses at rising edge.
--						  The encoded signals are sent to F_0 (HEV) and F_1 (Busy). 
-- ****************************************************************************

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
USE ieee.std_logic_misc.all;  -- Use OR_REDUCE function

USE work.v1495pkg.all;

ENTITY MainLogicUser IS
   PORT( 
      nLBRES      : IN     std_logic;                       -- Async Reset (active low)
      LCLK        : IN     std_logic;                       -- Local Bus Clock
      --*************************************************
      -- REGISTER INTERFACE
      --*************************************************
      REG_WREN    : IN     std_logic;                       -- Write pulse (active high)
      REG_RDEN    : IN     std_logic;                       -- Read  pulse (active high)
      REG_ADDR    : IN     std_logic_vector (15 DOWNTO 0);  -- Register address
      REG_DIN     : IN     std_logic_vector (15 DOWNTO 0);  -- Data from CAEN Local Bus
      REG_DOUT    : OUT    std_logic_vector (15 DOWNTO 0);  -- Data to   CAEN Local Bus
      USR_ACCESS  : IN     std_logic;                       -- Current register access is 
                                                            -- at user address space(Active high)
      --*************************************************
      -- V1495 Front Panel Ports (PORT A,B,C,G)
      --*************************************************
      A_DIN       : IN     std_logic_vector (31 DOWNTO 0);  -- In A (32 x LVDS/ECL)
      B_DIN       : IN     std_logic_vector (31 DOWNTO 0);  -- In B (32 x LVDS/ECL) 
      C_DOUT      : OUT    std_logic_vector (31 DOWNTO 0);  -- Out C (32 x LVDS)
      G_LEV       : OUT    std_logic;                       -- Output Level Select (NIM/TTL)
      G_DIR       : OUT    std_logic;                       -- Output Enable
      G_DOUT      : OUT    std_logic_vector (1 DOWNTO 0);   -- Out G - LEMO (2 x NIM/TTL)
      G_DIN       : IN     std_logic_vector (1 DOWNTO 0);   -- In G - LEMO (2 x NIM/TTL)
      --*************************************************
      -- A395x MEZZANINES INTERFACES (PORT D,E,F)
      --*************************************************
      -- Expansion Mezzanine Identifier:
      -- x_IDCODE :
      -- 000 : A395A (32 x IN LVDS/ECL)
      -- 001 : A395B (32 x OUT LVDS)
      -- 010 : A395C (32 x OUT ECL)
      -- 011 : A395D (8  x IN/OUT NIM/TTL)
      
      -- Expansion Mezzanine Port Signal Standard Select
      -- x_LEV : 
      --    0=>TTL,1=>NIM

      -- Expansion Mezzanine Port Direction
      -- x_DIR : 
      --    0=>OUT,1=>IN

      -- In/Out D (I/O Expansion)
      D_IDCODE    : IN     std_logic_vector ( 2 DOWNTO 0);  -- D slot mezzanine Identifier
      D_LEV       : OUT    std_logic;                       -- D slot Port Signal Level Select 
      D_DIR       : OUT    std_logic;                       -- D slot Port Direction
      D_DIN       : IN     std_logic_vector (31 DOWNTO 0);  -- D slot Data In  Bus
      D_DOUT      : OUT    std_logic_vector (31 DOWNTO 0);  -- D slot Data Out Bus
      -- In/Out E (I/O Expansion)
      E_IDCODE    : IN     std_logic_vector ( 2 DOWNTO 0);  -- E slot mezzanine Identifier
      E_LEV       : OUT    std_logic;                       -- E slot Port Signal Level Select
      E_DIR       : OUT    std_logic;                       -- E slot Port Direction
      E_DIN       : IN     std_logic_vector (31 DOWNTO 0);  -- E slot Data In  Bus
      E_DOUT      : OUT    std_logic_vector (31 DOWNTO 0);  -- E slot Data Out Bus
      -- In/Out F (I/O Expansion)
      F_IDCODE    : IN     std_logic_vector ( 2 DOWNTO 0);  -- F slot mezzanine Identifier
      F_LEV       : OUT    std_logic;                       -- F slot Port Signal Level Select
      F_DIR       : OUT    std_logic;                       -- F slot Port Direction
      F_DIN       : IN     std_logic_vector (31 DOWNTO 0);  -- F slot Data In  Bus
      F_DOUT      : OUT    std_logic_vector (31 DOWNTO 0);  -- F slot Data Out Bus
      --*************************************************
      -- DELAY LINES
      --*************************************************
      -- PDL = Programmable Delay Lines  (Step = 0.25ns / FSR = 64ns)
      -- DLO = Delay Line Oscillator     (Half Period ~ 10 ns)
      -- 3D3428 PDL (PROGRAMMABLE DELAY LINE) CONFIGURATION
      PDL_WR      : OUT    std_logic;                       -- Write Enable
      PDL_SEL     : OUT    std_logic;                       -- PDL Selection (0=>PDL0, 1=>PDL1)
      PDL_READ    : IN     std_logic_vector ( 7 DOWNTO 0);  -- Read Data
      PDL_WRITE   : OUT    std_logic_vector ( 7 DOWNTO 0);  -- Write Data
      PDL_DIR     : OUT    std_logic;                       -- Direction (0=>Write, 1=>Read)
      -- DELAY I/O
      PDL0_OUT    : IN     std_logic;                       -- Signal from PDL0 Output
      PDL1_OUT    : IN     std_logic;                       -- Signal from PDL1 Output
      DLO0_OUT    : IN     std_logic;                       -- Signal from DLO0 Output
      DLO1_OUT    : IN     std_logic;                       -- Signal from DLO1 Output
      PDL0_IN     : OUT    std_logic;                       -- Signal to   PDL0 Input
      PDL1_IN     : OUT    std_logic;                       -- Signal to   PDL1 Input
      DLO0_GATE   : OUT    std_logic;                       -- DLO0 Gate (active high)
      DLO1_GATE   : OUT    std_logic;                       -- DLO1 Gate (active high)
      --*************************************************
      -- SPARE PORTS
      --*************************************************
      SPARE_OUT    : OUT   std_logic_vector(11 downto 0);   -- SPARE Data Out 
      SPARE_IN     : IN    std_logic_vector(11 downto 0);   -- SPARE Data In
      SPARE_DIR    : OUT   std_logic_vector(11 downto 0);   -- SPARE Direction (0 => OUT, 1 => IN)   
      --*************************************************
      -- LED
      --*************************************************
      RED_PULSE       : OUT    std_logic;                   -- RED   Led Pulse (active high)
      GREEN_PULSE     : OUT    std_logic                    -- GREEN Led Pulse (active high)
   );

-- Declarations

END MainLogicUser ;

ARCHITECTURE rtl OF MainLogicUser IS

-- Local Signals
signal A     : std_logic_vector(31 downto 0);
signal B     : std_logic_vector(31 downto 0);
signal C   	 : std_logic_vector(31 downto 0);

--signal counter : std_logic_vector(31 downto 0) := (others => '0');
signal green_led : std_logic := '0';

-- Busy 
signal BusyIN1 : std_logic_vector(31 downto 0);
signal BusyIN2 : std_logic_vector(31 downto 0);
signal BusyOUT	 : std_logic;
signal BUSYencod : std_logic;

-- HEV
signal HEVin : std_logic;
signal HEVencod : std_logic;

-- Final Veto output
signal VetoOUT : std_logic;

-- Component Declarations
COMPONENT BusyLogicComponent
PORT (
	LCLK:		IN		std_logic;	-- ADC clock
	BusyIN1 : IN		std_logic_vector(31 downto 0);
	BusyIN2 : IN		std_logic_vector(31 downto 0);
	BusyOUT : OUT	std_logic
);
END COMPONENT;

-- returns 
--	one pulses if 0=>1 transitions
--	two pulse for 1=>0 transitions
COMPONENT EncodingComponent
PORT( 
	LCLK:			IN		std_logic;	-- ADC clock
	SignalIN:	IN		std_logic;
	SignalOUT:	OUT	std_logic
);
END COMPONENT ;



BEGIN

   --******************** put levels to NIM *************
	F_LEV <= '1'; -- set to NIM 
	G_LEV <= '1'; -- set to NIM 
   
   --******************** SPARE PORT ********************
   -- ALL Outputs driving 0
   SPARE_OUT <= (others => '0');
   SPARE_DIR <= (others => '1'); 

   --******************** LED PULSES ********************
--   RED_PULSE   <= green_led;
	
   --******************** Inputs ************************
	BusyIN1 <= A_DIN;
	BusyIN2 <= B_DIN; -- not used
	HEVin <= not G_DIN(0); -- HEVin = '1' if NIM pulse is on
	--PulserIN <= not G_DIN(1);
	
		
	-- check if any of the digitizers is busy
	-- If so, then BusyOUT is '1', else BusyOUT='0'
	busy_logic : BusyLogicComponent
	PORT MAP (
		LCLK => LCLK,
		BusyIN1  => BusyIN1,
		BusyIN2 => BusyIN2,
		BusyOUT => BusyOUT
	);
	
	-- Encodeing the HEV signal as follows:
	-- => one pulse (10ns) at falling edge and
	-- 	two pulses (10 ns each) at rising edge.
	HEV_encoding : EncodingComponent
	PORT MAP (
		LCLK => LCLK,
		SignalIN => HEVin,
		SignalOUT => HEVencod
	);

	-- Encodeing the BusyOUT signal as follows:
	-- => one pulse (10ns) at falling edge and
	-- 	two pulses (10 ns each) at rising edge.
	BUSY_encoding : EncodingComponent
	PORT MAP (
		LCLK => LCLK,
		SignalIN => BusyOUT,
		SignalOUT => BUSYencod
	);
		
	-- combine Busy veto and HEV with OR logic to final VetoOUT
	process (LCLK) begin
		if LCLK'event and LCLK = '1' then
			if BusyOUT = '1' OR HEVin = '1' then -- LVDS Busy is active low signal
				VetoOUT <= '1';
			else
				VetoOUT <= '0';
			end if;
		end if;
	end process;
	
   --******************** Outputs **************************
	F_DOUT(0) <=  HEVencod; -- to acquisition monitor
	F_DOUT(1) <=  BUSYencod; -- to acquisition monitor
	F_DOUT(2) <=  VetoOUT; -- final veto output
	GREEN_PULSE <= VetoOUT; -- green LED turns on, if veto is on
	
   
END rtl;

